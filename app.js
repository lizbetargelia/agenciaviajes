const http = require("http");
const express = require("express");
const { dirname } = require("path");
const app = express();
const bodyparser = require("body-parser");
const misRutas = require("./router/index")
const path = require("path");


app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));
app.use(misRutas);

app.engine('html', require('ejs').renderFile)



app.use((req,res,next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');

});
//escuchar el servidor por el puerto 502
const puerto = 502;
app.listen(puerto, ()=>{

    console.log("Iniciado puerto 502");
});
