const express = require("express");
const router = express.Router();
const bodyParse = require("body-parser");


//agencia viajes
router.get("/agenciaViajes",(req,res)=>{
    const valores = {
        tipoVia:req.query.tipoVia,
        precio:req.query.precio,
        numBole:req.query.numBole,
        nomCliente:req.query.nomCliente,
        edad:req.query.edad,
        destino:req.query.destino
       
        
    
    }
    res.render('agenciaViajes.html',valores);
});

router.post("/agenciaViajes",(req,res)=>{
    const valores = {
        tipoVia:req.body.tipoVia,
        precio:req.body.precio,
        numBole:req.body.numBole,
        nomCliente:req.body.nomCliente,
        edad:req.body.edad,
        destino:req.body.destino
        
    }
    res.render('agenciaViajes.html',valores);
});

module.exports = router;